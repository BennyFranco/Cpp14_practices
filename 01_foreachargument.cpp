#include <iostream>

// It invokes a callable object on every passed argument.
template <class F, class... Args>
void for_each_argument(F f, Args &&... args)
{
    [](...) {}((f(std::forward<Args>(args)), 0)...);
}

int main()
{
    //Print hello123
    for_each_argument(
        [](const auto &x) { std::cout << x; },
        "hello",
        1,
        2,
        3);
    std::cout << std::endl;
}