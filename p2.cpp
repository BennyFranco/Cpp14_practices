#include <iostream>

// It invokes a callable object on every passed argument.
template <typename TF, typename... Ts>
void forArgs(TF&& mFn, Ts&&... mArgs)
{
    return (void)std::initializer_list<int>
    {
        (
            mFn(std::forward<Ts>(mArgs)), 
            0
        )...
    };
}

int main()
{
    //Print hello123
    forArgs(
        [](const auto &x) { std::cout << x; },
        "hello",
        1,
        2,
        3);
    std::cout << std::endl;
}